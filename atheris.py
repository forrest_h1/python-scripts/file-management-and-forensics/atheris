#!/usr/bin/python3
#
#Atheris - Simple EXIF data reader for the CLI
#
#Written by Forrest Hooker, 04/11/2023

from PIL import Image, ExifTags
import argparse
import sys, os

AtHeader = ("Atheris v0.1 - https://gitlab.com/forrest_h1/python-scripts/file-management-and-forensics/atheris")

#Used for checking if file is even supposed to have EXIF data
supportedExts=[
        ".jpg",
        ".jpeg",
        ".TIF",
        ".webp"
        ]

#EXIF is not a standard for PNG but can exist
possibleExt=(".png")

#Class for making print colors/formatting easier
class termf:
    RESET = '\033[0m'
    BOLD = '\033[1m'
    U_LINE = '\033[4m'
    GREEN = '\033[32m'
    RED = '\033[31m'
    YELLOW = '\033[33m'
    WHITE = '\033[37m'
    B_GREEN = ('{}{}'.format(BOLD, GREEN))
    B_RED = ('{}{}'.format(BOLD, RED))
    B_WHITE = ('{}{}'.format(BOLD, WHITE))
    R_GREEN = ('{}{}'.format(RESET, GREEN))
    R_RED = ('{}{}'.format(RESET, RED))
    R_WHITE = ('{}{}'.format(RESET, WHITE))


#Bleck.
def __Main__():
    __argCheck__()
    __exifDump__()



#Used to check args for validity
def __argCheck__():
    global imgFile
    #Print Header
    print("{}{}{}{}".format(termf.BOLD, termf.WHITE, AtHeader, termf.RESET))
    #If no argument given, notify user & exit
    if len(sys.argv) == 1:
        print("\n{}No argument given.{}\n".format(termf.RED, termf.RESET))
        __Usage__()
        sys.exit()
    #Else, if arguments given...
    else:

        ##  Parser Section  ##

        parser = argparse.ArgumentParser()
        #We PROBABLY don't need to do it this way, but I wanted to make it easier to request specific fields later.
        parser.add_argument('-f', '--file',
                dest='imgFile',
                help='Specify an image file to read EXIF data from'
                )
        args = parser.parse_args()
        
        #If any args given...
        if args:
            #Set imgFile to the option given to the argParser
            imgFile = args.imgFile
            #If file does not even exist, notify user and exit.
            if not os.path.isfile(imgFile):
                print("\n{}File not found in specified directory.{}\n".format(termf.RED, termf.RESET))
                sys.exit()
            #Else, if file DOES exist...
            else:
                #Grab file extension for checking
                imgExt = os.path.splitext(imgFile)[1]
                #If imgExt is in KNOWN SUPPORTED extensions, notify user (With GREEN!)
                if imgExt in supportedExts:
                    print("\n{}Known filetype {}{}{} Detected.{}\n".format(termf.GREEN,termf.BOLD,imgExt,termf.R_GREEN,termf.RESET))
                #Elif imgExt is a PNG, notify user that EXIF data is not a given.
                elif imgExt in possibleExt:
                    print("{}{} Detected. EXIF Data possibly not present.{}".format(termf.YELLOW, imgExt, termf.RESET))
                #Else, tell user this is probably not an EXIF-supported file and exit
                else:
                    print("{}Unsupported file type. {} does not use EXIF data.{}".format(termf.RED, imgExt, termf.RESET))
                    sys.exit()

def __exifDump__():
    #Set img to the actual data within the image file
    img = Image.open(imgFile)
    #set imgEXIF to whatever exif data can be found within imgFile
    imgEXIF = img.getexif()
    #If no EXIF data is found, notify user and exit.
    if imgEXIF is None:
        print("{}No EXIF data for {}{}{} Found.{}\n\n".format(termf.RED, termf.BOLD, imgFile, termf.R_RED, termf.RESET))
        sys.exit()
    #Else, if EXIF data is found...
    else:
        for key, val in imgEXIF.items():
            #If val is type Bytes, try to decode - else, leave alone.
            try:
                #set val to decoded version of itself
                val = val.decode()
            except (UnicodeDecodeError, AttributeError):
                pass
            #Print the key-value pair
            print(f'{termf.B_WHITE}{ExifTags.TAGS[key]}: {termf.R_WHITE}{val}{termf.RESET}')
        #Bad formatting from Forrest :)
        print("")


## Sub-Functions ##

def __Usage__():
    print("Usage: -f <file_name>")
    print("Supported file extensions: JPG, JPEG, TIF, WEBP, PNG* (*Does not necessarily always have EXIF data)\n")
    



__Main__()
